# README #

O Projeto foi desenvolvido utilizando .NET core versão 1.1.

Na Solução há dois projetos:

* DesafioMarte
Servidor Web API e um cliente html para testes.

* DesafioMarte.Test
Teste TDD do projeto.

Endereço do cliente para testes:
Http://localhost:{porta}/index.html