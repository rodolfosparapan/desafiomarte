using System;
using Xunit;
using DesafioMarte.Exceptions;
using System.Collections.Generic;
using DesafioMarte.Services;
using DesafioMarte.Helpers;

namespace DesafioMarte.Tests
{
    public class DesafioMarteTest
    {
        private Probe probe1;
        private Probe probe2;

        public DesafioMarteTest()
        {
            MeshHelper.SetMeshCoordenate(5, 5);
            this.probe1 = new Probe().SetCoordinates(1, 2).SetDirection('N').SetInstructions("LMLMLMLMM");
            this.probe2 = new Probe().SetCoordinates(3, 3).SetDirection('E').SetInstructions("MMRMMRMRRM");
        }

        [Fact]
        public void ValidSingleMove()
        {
            ProbeProcess process = new ProbeProcess(new List<Probe>() { this.probe1 });
			Assert.True(process.Execute());

            Assert.Equal("1 3 N", this.probe1.GetCurrentPosition());
        }

		[Fact]
		public void ValidMultipleMove()
		{
            ProbeProcess process = new ProbeProcess(new List<Probe>() { this.probe1, this.probe2 });
			Assert.True(process.Execute());

			Assert.Equal("1 3 N", this.probe1.GetCurrentPosition());
            Assert.Equal("5 1 E", this.probe2.GetCurrentPosition());
		}

		[Fact]
		public void InvalidMoveNegativeArea()
		{
			Probe probe = this.probe1.SetInstructions("RRMMMMMMM");

			Exception ex = Assert.Throws<InvalidCoordinateException>(
				() => {
					ProbeProcess process = new ProbeProcess(new List<Probe>() { probe });
					process.Execute();
				}
				);

			Assert.Equal("The new probe position (1,-1), Exceeds the mesh area! [0,0] -> [5,5]", ex.Message);
		}

		[Fact]
		public void InvalidMoveOutMeshArea()
		{
			Probe probe = this.probe2.SetInstructions("MMMMMMMMMMMM");

			Exception ex = Assert.Throws<InvalidCoordinateException>(
				() => {
					ProbeProcess process = new ProbeProcess(new List<Probe>() { probe });
					process.Execute();
				}
				);

			Assert.Equal("The new probe position (6,3), Exceeds the mesh area! [0,0] -> [5,5]", ex.Message);
		}
    }
}
