﻿using System;
namespace DesafioMarte.Commands
{
    public class TurnRight : ICommand
    {
		public void Execute(Probe probe)
		{
			char newDirection = probe.GetDirectionObject().TurnRight();
			probe.SetDirection(newDirection);
		}
    }
}
