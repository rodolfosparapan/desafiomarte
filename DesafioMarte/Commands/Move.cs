﻿using System;
using DesafioMarte.Helpers;

namespace DesafioMarte.Commands
{
    public class Move : ICommand
    {
        public void Execute(Probe probe)
        {
            probe.GetDirectionObject().Move(probe);    
            MeshHelper.ValidateMeshLimit(probe.CoordX, probe.CoordY);
        }
    }
}
