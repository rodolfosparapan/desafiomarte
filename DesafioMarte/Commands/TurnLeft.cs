﻿using System;
namespace DesafioMarte.Commands
{
    public class TurnLeft : ICommand
    {
        public void Execute(Probe probe)
        {
            char newDirection = probe.GetDirectionObject().TurnLeft();
            probe.SetDirection(newDirection);    
        }
    }
}
