﻿using System;
namespace DesafioMarte.Commands
{
    public interface ICommand
    {
        void Execute(Probe probe);
    }
}
