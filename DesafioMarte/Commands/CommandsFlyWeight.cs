﻿using System;
using System.Collections.Generic;

namespace DesafioMarte.Commands
{
    public class CommandsFlyWeight
    {
        private Dictionary<char, ICommand> commands;

        public CommandsFlyWeight()
        {
            this.commands = new Dictionary<char, ICommand>(){
                {'L', new TurnLeft()},
                {'R', new TurnRight()},
                {'M', new Move()}
            };
        }

        public ICommand GetCommand(char command)
        {
            return commands[command];    
        }
    }
}
