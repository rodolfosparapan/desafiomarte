﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DesafioMarte.Models
{
    public class RequestData
    {
        [Range(0, 999)]
        public int UpperCordX { get; set; }

        [Range(0, 999)]
        public int UpperCordY { get; set; }

        public List<ProbeRequest> Probes { get; set; }
    }

    public class ProbeRequest
    {
        [Range(0, 999)]
        public int CoordX { get; set; } = 0;

        [Range(0, 999)]
        public int CoordY { get; set; } = 0;

        [RegularExpression("^(N|S|E|W)$", ErrorMessage = "Invalid Direction! The directions must be one of the followings: [N,S,E,W]")]
        public char Direction { get; set; } = 'N';

        [RegularExpression("^(L|R|M)*$", ErrorMessage = "Invalid Instruction! The instructions must be one of the followings: [L,R,M]")]
        public string Instructions { get; set; } = "";
    }
}
