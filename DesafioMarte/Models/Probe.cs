﻿using System;
using DesafioMarte.Commands;
using DesafioMarte.Directions;
using DesafioMarte.Helpers;

namespace DesafioMarte
{
    public class Probe
    {
        public int CoordX { get; private set; } = 0;
		public int CoordY { get; private set; } = 0;
		public char direction = 'N';
        public string instructions = "";

		public Probe SetCoordinates(int cordX, int cordY)
		{
			this.CoordX = cordX;
			this.CoordY = cordY;
			return this;
		}

		public Probe SetDirection(char direction)
		{
            this.direction = direction;
			return this;
		}

		public Probe SetInstructions(string instructions)
		{
            this.instructions = instructions;
			return this;
		}

        public IDirection GetDirectionObject(){

            return DirectionFactory.GetInstance(this.direction);
        }

        public string GetInstructions()
        {
            return this.instructions;
        }

        public void MoveNorth()
        {
            this.CoordY++;
        }

		public void MoveSouth()
		{
            this.CoordY--;
        }

		public void MoveWest()
		{
            this.CoordX--;
		}

		public void MoveEast()
		{
            this.CoordX++;
        }

		public string GetCurrentPosition()
		{
            return String.Format("{0} {1} {2}", this.CoordX, this.CoordY, this.direction);
		}
    }
}
