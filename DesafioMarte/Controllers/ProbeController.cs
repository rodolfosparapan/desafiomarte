﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DesafioMarte.Helpers;
using DesafioMarte.Models;
using DesafioMarte.Services;
using Microsoft.AspNetCore.Mvc;

namespace DesafioMarte.Controllers
{
    [Route("api/[controller]")]
    public class ProbeController : Controller
    {
        [HttpPost]
        public IActionResult Post([FromBody]RequestData requestData)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            MeshHelper.SetMeshCoordenate(requestData.UpperCordX, requestData.UpperCordY);

            List<Probe> probes = ProbeHelper.ParseToProbeModel(requestData.Probes);
            ProbeProcess probeProcess = new ProbeProcess(probes);

            try
            {
                probeProcess.Execute();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }

            return Ok(probeProcess.GetResponse());
        }

        [HttpGet]
		public String Get()
		{
            return "teste!";
		}
    }
}