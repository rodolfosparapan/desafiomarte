﻿using System;
using System.Collections.Generic;
using DesafioMarte.Commands;

namespace DesafioMarte.Services
{
    public class ProbeProcess
    {
        private List<Probe> probes;
        private CommandsFlyWeight commands;

		public ProbeProcess(List<Probe> probes)
		{
			this.probes = probes;
			this.commands = new CommandsFlyWeight();
		}

        public bool Execute()
        {
            foreach(Probe probe in probes)
            {
                this.ExecuteProbeInstructions(probe);
            }
            return true;
        }

        private void ExecuteProbeInstructions(Probe probe)
        {
			for (int i = 0; i < probe.GetInstructions().Length; i++)
			{
				char letter = probe.GetInstructions().Substring(i, 1)[0];
				ICommand command = this.commands.GetCommand(letter);
				command.Execute(probe);
			}
        }

		public string GetResponse()
		{
			List<string> probesReponse = new List<string>();
			foreach (Probe probe in this.probes)
			{
				probesReponse.Add(probe.GetCurrentPosition());
			}
			return String.Join("\n", probesReponse.ToArray());
		}
    }
}