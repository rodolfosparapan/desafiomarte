﻿using System;
namespace DesafioMarte.Directions
{
    public class East : IDirection
    {
		public char TurnLeft()
        {
            return 'N';
        }

        public char TurnRight()
		{
            return 'S';
        }

		public void Move(Probe probe)
		{
            probe.MoveEast();
		}
    }
}
