﻿using System;

namespace DesafioMarte.Directions
{
    public class DirectionFactory
    {
        public static IDirection GetInstance(char direction)
        {
            switch(direction)
            {
                case 'N': return new North();
                case 'S': return new South();
                case 'E': return new East();
                case 'W': return new West();
            }

            return new North();
        }
    }
}
