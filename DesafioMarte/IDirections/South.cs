﻿using System;
namespace DesafioMarte.Directions
{
    public class South : IDirection
    {
		public char TurnLeft()
		{
            return 'E';
		}

		public char TurnRight()
		{
            return 'W';
		}

		public void Move(Probe probe)
		{
            probe.MoveSouth();
		}
    }
}
