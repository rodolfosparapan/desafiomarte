﻿using System;
namespace DesafioMarte.Directions
{
    public class West : IDirection
    {
		public char TurnLeft()
		{
            return 'S';
		}

		public char TurnRight()
		{
            return 'N';
		}

		public void Move(Probe probe)
		{
            probe.MoveWest();
		}
    }
}
