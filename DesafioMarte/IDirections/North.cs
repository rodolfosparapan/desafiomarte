﻿using System;
namespace DesafioMarte.Directions
{
    public class North : IDirection
    {
		public char TurnLeft()
		{
            return 'W';
		}

		public char TurnRight()
		{
            return 'E';
		}

        public void Move(Probe probe)
        {
            probe.MoveNorth();
        }
    }
}
