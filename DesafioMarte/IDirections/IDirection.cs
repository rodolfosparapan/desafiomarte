﻿using System;
namespace DesafioMarte.Directions
{
    public interface IDirection
    {
		char TurnLeft();
        char TurnRight();

        void Move(Probe probe);
    }
}
