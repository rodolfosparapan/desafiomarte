﻿using System;
namespace DesafioMarte.Exceptions
{
    public class InvalidCoordinateException : Exception
    {
        public InvalidCoordinateException(string message)
		: base(message)
		{
		}
    }
}
