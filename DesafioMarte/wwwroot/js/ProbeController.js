﻿
function sendProbes(event) 
{
    event.preventDefault();
    
    $.ajax({
        type: "POST",
        data: JSON.stringify(getProbesData()),
        url: "api/probe",
        contentType: "application/json"
    })
    .success(function(data) {
        $('#server-response').html(data.replace('\n', '<br/>'));
    })
    .error(function(data) {
        $('#server-response').html(data.responseText);
    })
    .always(function() {
        $('.modal').modal('show');
    });
}