﻿
function getProbesData() 
{
    var arrayProbes = [];
    var arrayProbesInput = $('#probes input');
    for(var i=0; i < arrayProbesInput.length; i+=4)
    {
        arrayProbes.push({
                    coordX: arrayProbesInput[i].value,
                    coordY: arrayProbesInput[i+1].value,
                    direction: arrayProbesInput[i+2].value,
                    instructions : arrayProbesInput[i+3].value,
                });
    }

    return {
        upperCordX: $('#upperCoordX').val(), 
        upperCordY: $('#upperCoordY').val(), 
        probes : arrayProbes
    };
}

function getTestData() 
{    
    return {
        upperCordX: 5, 
        upperCordY: 5, 
        probes: [{
            coordX: 1,
            coordY: 2,
            direction: 'N',
            instructions: 'LMLMLMLMM'
        }, {
            coordX: 3,
            coordY: 3,
            direction: 'E',
            instructions: 'MMRMMRMRRM'
        }]
    };
}
