﻿
var probeTemplate = `<div class="row">
                        <div class="col-2">
                            <input type="text" name="cordX" placeholder="Coord X" class="form-control" required/>
                        </div>
                        <div class="col-2">
                            <input type="text" name="cordY" placeholder="Coord Y" class="form-control" required/>
                        </div>
                        <div class="col-2">
                            <input type="text" name="Direction" placeholder="Direction" class="form-control" required/>
                        </div>
                        <div class="col-3">
                            <input type="text" name="moveToward" placeholder="Move Torward" class="form-control" required/>
                        </div>
                    </div>`;

function newProbeLine(){
    $('#probes').append(probeTemplate);
}

function loadProbes(qtdProbes){

    $('#probes').html('');
    for(var i=0; i<qtdProbes; i++)
    {
        newProbeLine();
    }
}