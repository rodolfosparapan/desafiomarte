﻿using System;
using System.Collections.Generic;
using DesafioMarte.Models;

namespace DesafioMarte.Helpers
{
    public class ProbeHelper
    {
        public static List<Probe> ParseToProbeModel(List<ProbeRequest> probesRequest)
        {
            List<Probe> probesModel = new List<Probe>();
            foreach(ProbeRequest probe in probesRequest)
            {
                probesModel.Add(new Probe()
                           .SetCoordinates(probe.CoordX, probe.CoordY)
                           .SetDirection(probe.Direction)
                           .SetInstructions(probe.Instructions)
                          );
            }
            return probesModel;
        }
    }
}
