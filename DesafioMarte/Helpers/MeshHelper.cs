﻿using System;
using DesafioMarte.Exceptions;

namespace DesafioMarte.Helpers
{
    public class MeshHelper
    {
        private static int coordX = 0;
        private static int coordY = 0;

        public static void SetMeshCoordenate(int coordX, int coordY)
        {
            MeshHelper.coordX = coordX;
            MeshHelper.coordY = coordY;
        }

        public static void ValidateMeshLimit(int newCoordX, int newCoordY)
        {
            if (newCoordX > MeshHelper.coordX || newCoordX < 0 ||
                newCoordY > MeshHelper.coordY || newCoordY < 0){

				throw new InvalidCoordinateException(
                    String.Format("The new probe position ({0},{1}), Exceeds the mesh area! [0,0] -> [{2},{3}]",
								   newCoordX, newCoordY, MeshHelper.coordX, MeshHelper.coordY));
            }
       }
    }
}
